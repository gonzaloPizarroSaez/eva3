package com.mycompany.eva3ciisa;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configures JAX-RS for the application.
 * @author Juneau
 */

@ApplicationPath("api")
public class AppConfig extends Application {}
