/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eva3.services;

import com.mycompany.eva3ciisa.dao.ComunasJpaController;
import com.mycompany.eva3ciisa.entity.Comunas;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author gps
 */
@Path("comunas")
public class ComunaRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)

    public Response listarComunas() {

        ComunasJpaController dao = new ComunasJpaController();
        List<Comunas> comunas = dao.findComunasEntities();
        return Response.ok(200).entity(comunas).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Comunas comuna) {

        ComunasJpaController dao = new ComunasJpaController();
        try {
            dao.create(comuna);
        } catch (Exception ex) {
            Logger.getLogger(ComunaRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(comuna).build();

    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Comunas comuna) {
        ComunasJpaController dao = new ComunasJpaController();
        try {
            dao.edit(comuna);
        } catch (Exception ex) {
            Logger.getLogger(ComunaRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(comuna).build();
    }

    @DELETE
    @Path("/{ideliminar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eiminar(@PathParam("ideliminar") String ideliminar) {
        ComunasJpaController dao = new ComunasJpaController();

        try {
            dao.destroy(ideliminar);
        } catch (Exception ex) {
            Logger.getLogger(ComunaRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok("comuna eliminada").build();
    }

    @GET
    @Path("/{idconsulta}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarPorId(@PathParam("idconsulta") String idconsulta) {
        ComunasJpaController dao = new ComunasJpaController();

        Comunas comuna = dao.findComunas(idconsulta);
        return Response.ok(200).entity(comuna).build();
    }

}
